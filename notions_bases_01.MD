# Docker 

# Définition Docker  
c'est une plateforme open source qui permet de créer, de déployer et de gérer des applications dans des conteneurs légers et isolés.  

# Définition Conteneur  
C'est une unité d'exécution isollée qui regroupe une application et ces dépendances. Contrairement aux machines virtuelles traditoinnelles. Les conteneurs ne nécessitent pas d'émulation matérielle complète.  
Ils partagent le même noyau du système d'exploitation de l'hôte. Ce qui les rend plus légers et plus rapide à démarrer. Les conteneurs fournissent une isolation des ressources et un environement cohérent pour l'exécution des applications.  

# Moteur Docker (Docker engine)  
C'est le coeur de l'écosystème Docker. Il est responsable de la gestion des conteneurs, création des images. Ainsi que la communication avec le système d'exploitation hôte.  
Le moteur Docker fournit une API qui permet aux utilisateurs d'intéragir avec les conteneurs et gérer les opérations telles que le démarrage, l'arrêt et la suppression.  

## Images Docker  
C'est un modèle immuable qui contient tous les fichiers et les dépendances nécessaires pour exéctuer une application.

## Registe Docker  
C'est un service qui permet de stocker, gérer et distribuer des images Docker. Il exite des registes public tel que Docker Hub, qui hébergent de nombreuses images prêtes à l'emploi. Il possible de créer des registres privées pour stocker les images spécifiques à une entreprise ou à un proejet.  

## Volume Docekr  
C'est un mécanisme permmettant de stocker des données en dehors du conteneur. Les volumes sont utiles pour la persistance des données, la sauvegarde et le partage d'informations entre les conteneurs.

